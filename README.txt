
Start Provider:

python provider.py

Optional: Importing data from config files:

python provider.py "/home/mtasr/LT2.0/ltpipeline-dev-christian/*/config*"

Register server:

curl -H "Content-Type: application/json" '127.0.0.1:5000/register' -d '{"component": "asr", "name": "asr_server_7eu", "data": "http://192.168.0.60:5052/asr/infer/None,None"}'

Unregister server:

curl -H "Content-Type: application/json" '127.0.0.1:5000/unregister' -d '{"component": "asr", "name": "asr_server_7eu"}'

