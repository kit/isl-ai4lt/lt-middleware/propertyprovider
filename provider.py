
from flask import Flask, request
import pickle
import os
from glob import glob
import json
import requests
import threading
import time
import sys

# Create a Flask application
app = Flask(__name__)

if os.path.isfile('data.pickle'):
    with open('data.pickle', 'rb') as f:
        data = pickle.load(f)
else:
    data = {}

def save():
    with open('data_new.pickle', 'wb') as f:
        pickle.dump(data, f)
    os.rename('data_new.pickle','data.pickle')

def is_available(url):
    try:
        res = requests.post(url, verify=False)
        available = True
    except requests.exceptions.ConnectionError:
        available = False
        print("Not available:", url)
    return available

def register_name(component, name, d):
    if not component in data:
        data[component] = {}
    data[component][name] = {"data": d}
    av = False
    if type(d) is str and "server" in name and "http" in d:
        av = is_available(d)
        data[component][name]["available"] = av
    save()
    return av

def unregister_name(component, name):
    if component in data:
        if name in data[component]:
            del data[component][name]
            save()
            return True
    return False

# curl -H "Content-Type: application/json" '127.0.0.1:5000/register' -d '{"component": "asr", "name": "asr_server_7eu", "data": "http://192.168.0.60:5052/asr/infer/None,None"}'

@app.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    component, name, data = data["component"], data["name"], data["data"]

    av = register_name(component, name, data)
    return f"{component = } {name = } {data = } registered, available: {av}\n"

# curl -H "Content-Type: application/json" '127.0.0.1:5000/unregister' -d '{"component": "asr", "name": "asr_server_7eu"}'

@app.route('/unregister', methods=['POST'])
def unregister():
    data = request.get_json()
    component, name = data["component"], data["name"]

    if unregister_name(component, name):
        return f"{component = } {name = } unregistered.\n"
    else:
        return f"{component = } {name = } unregistering failed.\n", 400

def update_availability(minutes=10):
    while True:
        time.sleep(minutes*60)
        for component, d in data.items():
            for name, d2 in d.items():
                if "available" in d2:
                    d2["available"] = is_available(d2["data"])
        save()

def get_available_languages():
    output = {}
    for component, d in data.items():
        output[component] = []
        for name, d2 in d.items():
            if "available" in d2 and d2["available"]:
                name2 = name.split("server_")
                if len(name2) == 2:
                    output[component].append(name2[1])
                else:
                    output[component].append("mult")

        if component == "asr":
            names = output[component]
            names = [n for n in names if n=="ende"]+[n for n in names if n!="ende"]
            output[component] = names

    return output

@app.route('/get_available_languages', methods=['GET'])
def get_available_languages_():
    return get_available_languages(), 200

def get_single_inputlanguages():
    asr_langs = get_available_languages()["asr"]
    return [l for l in asr_langs if len(l)==2]

@app.route('/get_single_inputlanguages', methods=['GET'])
def get_single_inputlanguages_():
    return get_single_inputlanguages(), 200

def input_languages_to_lang(input_langs):
    if len(input_langs) == 1:
        return input_langs[0]

    asr_langs = get_available_languages()["asr"]
    asr_langs_single = [l for l in asr_langs if len(l) == 2]
    asr_langs_mult = [l for l in asr_langs if len(l) > 2]

    if len(input_langs) == 2 and "en" in input_langs and "de" in input_langs:
        for lang in ["ende","7eu","mult57"]:
            if lang in asr_langs_mult:
                return lang
    if len(input_langs) >= 3:
        for lang in ["mult57","7eu"]:
            if lang in asr_langs_mult:
                return lang

    if "en" in asr_langs_single:
        return "en"
    if len(asr_langs_single) > 0:
        return asr_langs_single[0]
    return "en"

@app.route('/input_languages_to_lang', methods=['GET'])
def input_languages_to_lang_():
    data = request.get_json()
    if not data or "input_langs" not in data:
        return "ERROR: No input_langs found", 400
    input_langs = data["input_langs"]
    return input_languages_to_lang(input_langs), 200

@app.route('/get_server/<component>/<language>', methods=['GET'])
def get_server(component, language):
    if component not in data:
        return f"{component = } not found\n", 400
    elif language not in data[component]:
        return f"{language = } of {component = } not found\n", 400
    return data[component][language], 200

if __name__ == '__main__':
    if len(sys.argv)>=2:
        print("Loading data from config files")

        update_from = sys.argv[1] #"/home/mtasr/LT2.0/ltpipeline-dev-christian/*/config*"

        for file in glob(update_from):
            component = file.split("/")[-2][len("streaming"):]
            with open(file, 'r') as f:
                config = json.load(f)
            for k,v in config.items():
                register_name(component, k, v)

    print(f"{data = }")

    thread = threading.Thread(target=update_availability)
    thread.start()

    # Run the Flask application
    app.run("0.0.0.0",debug=False)

